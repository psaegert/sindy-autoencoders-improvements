def sindy_library_tf_order2(z, dz, latent_dim, poly_order, include_sine=False):
    """
    Build the SINDy library for a second order system. This is essentially the same as for a first
    order system, but library terms are also built for the derivatives.
    """
    library = ['1']

    z_combined = z + dz

    for i in range(2*latent_dim):
        library.append(z_combined[i])

    if poly_order > 1:
        for i in range(2*latent_dim):
            for j in range(i,2*latent_dim):
                library.append(f'{z_combined[i]}*{z_combined[j]}')

    if poly_order > 2:
        for i in range(2*latent_dim):
            for j in range(i,2*latent_dim):
                for k in range(j,2*latent_dim):
                    library.append(f'{z_combined[i]}*{z_combined[j]}*{z_combined[k]}')

    if poly_order > 3:
        for i in range(2*latent_dim):
            for j in range(i,2*latent_dim):
                for k in range(j,2*latent_dim):
                    for p in range(k,2*latent_dim):
                        library.append(f'{z_combined[i]}*{z_combined[j]}*{z_combined[k]}*{z_combined[p]}')

    if poly_order > 4:
        for i in range(2*latent_dim):
            for j in range(i,2*latent_dim):
                for k in range(j,2*latent_dim):
                    for p in range(k,2*latent_dim):
                        for q in range(p,2*latent_dim):
                            library.append(f'{z_combined[i]}*{z_combined[j]}*{z_combined[k]}*{z_combined[p]}*{z_combined[q]}')

    if include_sine:
        for i in range(2*latent_dim):
            library.append(f'sin({z_combined[i]})')

    return library