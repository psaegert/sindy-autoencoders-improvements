# SINDy-Autoencoders Improvements

This repository contains the code for my bachelor thesis 'On Data-Driven Discovery Of Symbolic Differential Equations From Unsuitable Coordinates Using SINDy-Autoencoders', in which I conduct a verification and replication of the SINDy-Autoencoder by K. Champion et al. (https://arxiv.org/abs/1904.02107) and improve its reliability by replacing the Sequential Thresholding algorithm used to promote extra sparsity on the coefficients with a patient, trend-aware thresholding algorithm.

## The SINDy-Autoencoder
<img title="The SINDy-Autoencoder" alt="The SINDy-Autoencoder" src="./images/visual_abstract.png">
(from 'Data-driven discovery of coordinates and governing equations' by K. Champion et al. <a href="https://arxiv.org/abs/1904.02107">https://arxiv.org/abs/1904.02107</a>, edited)