import pysr
import numpy as np
import torch
import torch.nn as nn
from torch import Tensor
from torch.nn import MSELoss, L1Loss
from torch.optim import Adam

import Autoencoder
from Library import Library
from ActivityClassifier import ActivityClassifier

from trend import trend_verbose
from colortqdm import colortqdm


class MemoryDropout(nn.Dropout):
    def __init__(self, p=0.5):
        super().__init__()
        if p < 0 or p > 1:
            raise ValueError("dropout probability has to be between 0 and 1, but got {}".format(p))
        self.p = p
        self.scale = 1 / (1 - self.p)
        self.inverse_scale = (1 - self.p)
        self.drop_mask = None
        self.dropout = nn.Dropout(p)

    def forward(self, x, resample=True):
        if not self.training:
            return x
        
        if resample:
            self.drop_mask = self.dropout(torch.ones_like(x, device=x.device)).detach() * self.inverse_scale

        return self.drop_mask * x * self.scale


class TrajectoryDiscovery(nn.Module):
    def __init__(self):
        super(TrajectoryDiscovery, self).__init__()
    
    def transform_time_derivative_1st_order(self, T, x, dx, resample=False):
        dz = dx

        if len(T) == 0: return dz

        last_linear_layer_index = 0
        for i, layer in enumerate(T[:-1]):
            if layer._get_name() == 'Linear':
                x = layer(x)
                last_linear_layer_index = i
            elif layer._get_name() == 'MemoryDropout':
                x = layer(x, resample=resample)
            elif layer._get_name() == 'Dropout':
                x = layer(x)
            elif layer._get_name() == 'Sigmoid': # Activation
                x = layer(x)
                dz = torch.multiply(torch.multiply(x, 1 - x), torch.matmul(dz, T[last_linear_layer_index].weight.T))
            else:
                raise TypeError(f'Unsupported Layer: {layer._get_name()}')

        return torch.matmul(dz, T[-1].weight.T)

    def transform_time_derivative_2nd_order(self, T, x, dx, ddx, resample=False):
        dz = dx
        ddz = ddx

        if len(T) == 0: return dz, ddz

        last_linear_layer_index = 0
        for i, layer in enumerate(T[:-1]):
            if layer._get_name() == 'Linear':
                x = layer(x)
                last_linear_layer_index = i
            elif layer._get_name() == 'MemoryDropout':
                x = layer(x, resample=resample)
            elif layer._get_name() == 'Dropout':
                x = layer(x)
            elif layer._get_name() == 'Sigmoid': # Activation
                x = layer(x)
                dz_prev = torch.matmul(dz, T[last_linear_layer_index].weight.T)
                sigmoid_derivative = torch.multiply(x, 1 - x)
                sigmoid_derivative2 = torch.multiply(sigmoid_derivative, 1 - 2*x)

                dz = torch.multiply(sigmoid_derivative, dz_prev)
                ddz = torch.multiply(sigmoid_derivative2, torch.square(dz_prev)) + torch.multiply(sigmoid_derivative, torch.matmul(ddz, T[last_linear_layer_index].weight.T))
            else:
                raise TypeError(f'Unsupported Layer: {layer._get_name()}')

        dz = torch.matmul(dz, T[-1].weight.T)
        ddz = torch.matmul(ddz, T[-1].weight.T)

        return dz, ddz

    def forward(self):
        raise NotImplementedError
    
    def fit_equation(self):
        raise NotImplementedError


class SINDyTrajectoryDiscovery(TrajectoryDiscovery):
    def __init__(self, autoencoder:Autoencoder, library:Library, loss_weights:dict, thresholder:ActivityClassifier=None, warmup:int=-1, l1_complexity:bool=False, ae_weight_decay:int=0):
        super(TrajectoryDiscovery, self).__init__()
        self.autoencoder = autoencoder
        
        self.dim = self.autoencoder.latent_dim
        self.library = library
        assert 0 < self.library.diff_order <= 2
        
        if l1_complexity: 
            self.library.term_weights = self.library.term_weights.to(self.autoencoder.device)
        else:
            self.library.term_weights = torch.ones_like(self.library.term_weights, device=self.autoencoder.device)

        self.coefficients = nn.Parameter(torch.ones(self.library.n_terms, self.dim, device=self.autoencoder.device), requires_grad=True)
        self.coefficient_mask = nn.Parameter(torch.full_like(self.coefficients, True, device=self.coefficients.device), requires_grad=False)

        self.reconstruction_loss_fn = MSELoss(reduction='sum')
        self.equation_x_loss_fn = MSELoss(reduction='sum')
        self.equation_z_loss_fn = MSELoss(reduction='sum')
        self.coefficient_loss_fn = L1Loss(reduction='mean')

        self.reconstruction_loss_weight = loss_weights['reconstruction_loss_weight']
        self.equation_x_loss_weight = loss_weights['equation_x_loss_weight']
        self.equation_z_loss_weight = loss_weights['equation_z_loss_weight']
        self.coefficient_loss_weight = loss_weights['coefficient_loss_weight']
        self.ae_weight_decay = ae_weight_decay

        self.thresholder = thresholder
        self.warmup = warmup

        self.optimizer = None
        self.loss_history = None
        self.coefficient_history = None
        self.currently_active_coefficients_history = None
    
    def get_coefficients(self):
        return self.coefficients.detach().cpu().numpy()

    def get_coefficient_mask(self):
        return self.coefficient_mask.detach().cpu().numpy()

    def get_masked_coefficients(self):
        return self.coefficient_mask.detach().cpu().numpy() * self.coefficients.detach().cpu().numpy()


    def forward(self, x:Tensor, dx:Tensor, ddx:Tensor=None):
        # Encode
        z, x_hat = self.autoencoder(x)

        # Encode time derivative
        if self.library.diff_order == 1:
            dz = self.transform_time_derivative_1st_order(self.autoencoder.encoder, x, dx)
            library_variables = self.library.collect_derivatives(z, dz)

        elif self.library.diff_order == 2:
            dz, ddz = self.transform_time_derivative_2nd_order(self.autoencoder.encoder, x, dx, ddx)
            library_variables = self.library.collect_derivatives(z, dz, ddz)

        # Equation LHS is highest order derivative
        equation_z_LHS = library_variables[:, -self.library.dim:]

        # Calculate SINDy snapshot (and do not use highest order derivative)
        equation_z_RHS = torch.matmul(self.library.build_snapshot(library_variables[:, :-self.library.dim]), self.coefficients * self.coefficient_mask)

        if self.library.diff_order == 1:
            equation_x_RHS = self.transform_time_derivative_1st_order(self.autoencoder.decoder, z, equation_z_RHS)
        elif self.library.diff_order == 2:
            _, equation_x_RHS = self.transform_time_derivative_2nd_order(self.autoencoder.decoder, z, dz, equation_z_RHS)

        return x_hat, equation_x_RHS, equation_z_LHS, equation_z_RHS
    

    def fit_equation(self, x, dx, ddx=None, validation_data=None, lr=5e-5, n_epochs=1, refinement=False, batch_size=32):
        if self.optimizer is None: self.optimizer = Adam(params=self.parameters(), lr=lr, weight_decay=self.ae_weight_decay)

        if self.coefficient_history is None: self.coefficient_history = []
        if self.currently_active_coefficients_history is None: self.currently_active_coefficients_history = []

        if self.loss_history is None:
            self.loss_history = {
                'active_coefficients': [], 'coefficient': [],
                'reconstruction': [], 'equation_x': [], 'equation_z': []
            }
            if validation_data is not None:
                self.loss_history = {**self.loss_history, **{'val_' + k: [] for k in self.loss_history.keys() if (k not in ['active_coefficients', 'coefficient'])}}

        n_epochs_tqdm = colortqdm(range(n_epochs), desc=f'{"Refinement" if refinement else "Training"} Epoch')

        for epoch in n_epochs_tqdm:
            self.train()

            epoch_reconstruction_loss = 0
            epoch_equation_x_loss = 0
            epoch_equation_z_loss = 0

            N_train = 0
            for i in range(x.size(0) // batch_size):
                if self.library.diff_order == 1:
                    x_dxs_batch_tuple = (x[i*batch_size:(i+1)*batch_size], dx[i*batch_size:(i+1)*batch_size])
                if self.library.diff_order == 2:
                    x_dxs_batch_tuple = (x[i*batch_size:(i+1)*batch_size], dx[i*batch_size:(i+1)*batch_size], ddx[i*batch_size:(i+1)*batch_size])

                N_batch, D = x_dxs_batch_tuple[0].size()
                N_train += N_batch

                self.optimizer.zero_grad()
                
                # FORWARD
                x_hat, equation_x_RHS, equation_z_LHS, equation_z_RHS = self(*x_dxs_batch_tuple)

                # LOSS
                reconstruction_loss = self.reconstruction_loss_fn(x_dxs_batch_tuple[0], x_hat) / D
                equation_x_loss = self.equation_x_loss_fn(x_dxs_batch_tuple[-1], equation_x_RHS) / D # use highest order
                equation_z_loss = self.equation_z_loss_fn(equation_z_LHS, equation_z_RHS) / self.dim
                coefficient_loss = self.coefficient_loss_fn(self.library.term_weights * self.coefficients * self.coefficient_mask, torch.zeros_like(self.coefficients, device=self.coefficients.device))
                    
                loss = (
                    self.reconstruction_loss_weight * reconstruction_loss / N_batch
                    + self.equation_x_loss_weight * equation_x_loss / N_batch * int(epoch > self.warmup)
                    + self.equation_z_loss_weight * equation_z_loss / N_batch * int(epoch > self.warmup)
                    + self.coefficient_loss_weight * coefficient_loss * (1 - int(refinement)) * int(epoch > self.warmup)
                )

                # BACKWARD
                loss.backward()
                self.optimizer.step()

                # LOG
                epoch_reconstruction_loss += reconstruction_loss.detach()
                epoch_equation_x_loss += equation_x_loss.detach()
                epoch_equation_z_loss += equation_z_loss.detach()
                
            self.loss_history['reconstruction'].append(epoch_reconstruction_loss.cpu().numpy() / N_train)
            self.loss_history['equation_x'].append(epoch_equation_x_loss.cpu().numpy() / N_train)
            self.loss_history['equation_z'].append(epoch_equation_z_loss.cpu().numpy() / N_train)
            self.loss_history['coefficient'].append(coefficient_loss.detach().cpu().numpy())

            self.coefficient_history.append(self.get_coefficients())

            if validation_data is not None:
                with torch.no_grad():
                    self.eval()
                    epoch_val_reconstruction_loss = 0
                    epoch_val_equation_x_loss = 0
                    epoch_val_equation_z_loss = 0
                    N_val = 0
                    for i in range(validation_data[0].size(0) // batch_size):
                        if self.library.diff_order == 1:
                            x_dxs_batch_tuple = (validation_data[0][i*batch_size:(i+1)*batch_size], validation_data[1][i*batch_size:(i+1)*batch_size])
                        if self.library.diff_order == 2:
                            x_dxs_batch_tuple = (validation_data[0][i*batch_size:(i+1)*batch_size], validation_data[1][i*batch_size:(i+1)*batch_size], validation_data[2][i*batch_size:(i+1)*batch_size])
                        N_batch, D = x_dxs_batch_tuple[0].size()
                        N_val += N_batch
                        # FORWARD
                        x_hat, equation_x_RHS, equation_z_LHS, equation_z_RHS = self(*x_dxs_batch_tuple)

                        # LOSS
                        epoch_val_reconstruction_loss += self.reconstruction_loss_fn(x_dxs_batch_tuple[0], x_hat) / D
                        epoch_val_equation_x_loss += self.equation_x_loss_fn(x_dxs_batch_tuple[-1], equation_x_RHS) / D
                        epoch_val_equation_z_loss += self.equation_z_loss_fn(equation_z_LHS, equation_z_RHS) / self.dim

                    self.loss_history['val_reconstruction'].append(epoch_val_reconstruction_loss.detach().cpu().numpy() / N_val)
                    self.loss_history['val_equation_x'].append(epoch_val_equation_x_loss.detach().cpu().numpy() / N_val)
                    self.loss_history['val_equation_z'].append(epoch_val_equation_z_loss.detach().cpu().numpy() / N_val)


            # Classify Coefficient Activity
            if not refinement and self.thresholder is not None:
                deactivate_coefficient_mask, current_deactivate_coefficient_mask = self.thresholder.classify_converged(self.coefficient_history)
                deactivate_coefficient_mask = deactivate_coefficient_mask.to(self.coefficients.device)
                if current_deactivate_coefficient_mask is not None: current_deactivate_coefficient_mask = current_deactivate_coefficient_mask.to(self.coefficients.device)
                
                self.coefficient_mask.data = torch.logical_and(self.coefficients.detach(), torch.logical_not(deactivate_coefficient_mask))

            self.loss_history['active_coefficients'].append(int(torch.sum(self.coefficient_mask).cpu().numpy()))

            # Monitor
            if not refinement and self.thresholder is not None and current_deactivate_coefficient_mask is not None:
                currently_active_coefficients = int(torch.sum(torch.logical_and(self.coefficient_mask, torch.logical_not(current_deactivate_coefficient_mask))).cpu().numpy())
                self.currently_active_coefficients_history.append(currently_active_coefficients)

            postfix_dict = {}
            for k, v in self.loss_history.items():
                digits = '{:.1e}'.format(v[-1]) if k != 'active_coefficients' else str(int(v[-1]))
                postfix_dict[k] = digits
                
                if not refinement and k == 'active_coefficients' and self.thresholder is not None and current_deactivate_coefficient_mask is not None:
                    # monitor the actual and current trend
                    postfix_dict[k] += f'({currently_active_coefficients}{trend_verbose(self.currently_active_coefficients_history, lengthscale=10, tolerance=2)})'
                else:
                    # only monitor the actual trend
                    postfix_dict[k] += trend_verbose(v, lengthscale=10, tolerance=2)

            n_epochs_tqdm.set_postfix(postfix_dict)
            

class PySRTrajectoryDiscovery(TrajectoryDiscovery):
    def __init__(self, autoencoder:Autoencoder, equation_files:list, diff_order:int, loss_weights:dict, warmup:int=-1, ae_weight_decay:int=0):
        super(PySRDiscovery, self).__init__()
        self.autoencoder = autoencoder
        
        self.dim = self.autoencoder.latent_dim
        self.diff_order = diff_order

        self.equation_files = equation_files
        self.best_equations = self.get_best_equations()
        
        self.reconstruction_loss_fn = MSELoss(reduction='sum')
        self.equation_x_loss_fn = MSELoss(reduction='sum')
        self.equation_z_loss_fn = MSELoss(reduction='sum')

        self.reconstruction_loss_weight = loss_weights['reconstruction_loss_weight']
        self.equation_x_loss_weight = loss_weights['equation_x_loss_weight']
        self.equation_z_loss_weight = loss_weights['equation_z_loss_weight']
        self.ae_weight_decay = ae_weight_decay

        self.warmup = warmup

        self.optimizer = None
        self.loss_history = None

    def get_best_equations(self):
        # return [pysr.best_callable(pysr.get_hof(equation_file=eq_path, n_features=self.diff_order*self.dim, output_torch_format=True)) for eq_path in self.equation_files] # sill does not output torch format. bug?
        return [pysr.best_row(pysr.get_hof(equation_file=eq_path, n_features=self.diff_order*self.dim, output_torch_format=True))['torch_format'] for eq_path in self.equation_files]

    def simulation_step(self, z, numpy=False):
        return (np if numpy else torch).stack([eq(z) for eq in self.best_equations]).T

    def forward(self, x:Tensor, dx:Tensor, ddx:Tensor=None):
        # Encode
        z, x_hat = self.autoencoder(x)

        # Encode time derivative
        if self.diff_order == 1:
            dz = self.transform_time_derivative_1st_order(self.autoencoder.encoder, x, dx)
            variables = torch.cat([z, dz.reshape(z.shape[0], -1)], dim=1)

        elif self.diff_order == 2:
            dz, ddz = self.transform_time_derivative_2nd_order(self.autoencoder.encoder, x, dx, ddx)
            variables = torch.cat([z, dz.reshape(z.shape[0], -1), ddz.reshape(z.shape[0], -1)], dim=1)

        # Equation LHS is highest order derivative
        equation_z_LHS = variables[:, -self.dim:]

        # Calculate snapshot (and do not use highest order derivative)
        equation_z_RHS = self.simulation_step(variables[:, :-self.dim])

        if self.diff_order == 1:
            equation_x_RHS = self.transform_time_derivative_1st_order(self.autoencoder.decoder, z, equation_z_RHS)
        elif self.diff_order == 2:
            _, equation_x_RHS = self.transform_time_derivative_2nd_order(self.autoencoder.decoder, z, dz, equation_z_RHS)

        return x_hat, equation_x_RHS, equation_z_LHS, equation_z_RHS
    

    def fit_equation(self, x, dx, ddx=None, validation_data=None, lr=5e-5, n_epochs=1, batch_size=32):
        if self.optimizer is None: self.optimizer = Adam(params=self.parameters(), lr=lr, weight_decay=self.ae_weight_decay)

        if self.loss_history is None:
            self.loss_history = {
                'reconstruction': [], 'equation_x': [], 'equation_z': []
            }
            if validation_data is not None:
                self.loss_history = {**self.loss_history, **{'val_' + k: [] for k in self.loss_history.keys()}}

        n_epochs_tqdm = colortqdm(range(n_epochs), desc=f'Training Epoch')

        for epoch in n_epochs_tqdm:
            self.train()

            epoch_reconstruction_loss = 0
            epoch_equation_x_loss = 0
            epoch_equation_z_loss = 0

            N_train = 0
            for i in range(x.size(0) // batch_size):
                if self.diff_order == 1:
                    x_dxs_batch_tuple = (x[i*batch_size:(i+1)*batch_size], dx[i*batch_size:(i+1)*batch_size])
                if self.diff_order == 2:
                    x_dxs_batch_tuple = (x[i*batch_size:(i+1)*batch_size], dx[i*batch_size:(i+1)*batch_size], ddx[i*batch_size:(i+1)*batch_size])

                N_batch, D = x_dxs_batch_tuple[0].size()
                N_train += N_batch

                self.optimizer.zero_grad()
                
                # FORWARD
                x_hat, equation_x_RHS, equation_z_LHS, equation_z_RHS = self(*x_dxs_batch_tuple)

                # LOSS
                reconstruction_loss = self.reconstruction_loss_fn(x_dxs_batch_tuple[0], x_hat) / D
                equation_x_loss = self.equation_x_loss_fn(x_dxs_batch_tuple[-1], equation_x_RHS) / D # use highest order
                equation_z_loss = self.equation_z_loss_fn(equation_z_LHS, equation_z_RHS) / self.dim
                    
                loss = (
                    self.reconstruction_loss_weight * reconstruction_loss / N_batch
                    + self.equation_x_loss_weight * equation_x_loss / N_batch * int(epoch > self.warmup)
                    + self.equation_z_loss_weight * equation_z_loss / N_batch * int(epoch > self.warmup)
                )

                # BACKWARD
                loss.backward()
                self.optimizer.step()

                # LOG
                epoch_reconstruction_loss += reconstruction_loss.detach()
                epoch_equation_x_loss += equation_x_loss.detach()
                epoch_equation_z_loss += equation_z_loss.detach()
                
            self.loss_history['reconstruction'].append(epoch_reconstruction_loss.cpu().numpy() / N_train)
            self.loss_history['equation_x'].append(epoch_equation_x_loss.cpu().numpy() / N_train)
            self.loss_history['equation_z'].append(epoch_equation_z_loss.cpu().numpy() / N_train)


            if validation_data is not None:
                with torch.no_grad():
                    self.eval()
                    epoch_val_reconstruction_loss = 0
                    epoch_val_equation_x_loss = 0
                    epoch_val_equation_z_loss = 0
                    N_val = 0
                    for i in range(validation_data[0].size(0) // batch_size):
                        if self.diff_order == 1:
                            x_dxs_batch_tuple = (validation_data[0][i*batch_size:(i+1)*batch_size], validation_data[1][i*batch_size:(i+1)*batch_size])
                        if self.diff_order == 2:
                            x_dxs_batch_tuple = (validation_data[0][i*batch_size:(i+1)*batch_size], validation_data[1][i*batch_size:(i+1)*batch_size], validation_data[2][i*batch_size:(i+1)*batch_size])
                        N_batch, D = x_dxs_batch_tuple[0].size()
                        N_val += N_batch
                        # FORWARD
                        x_hat, equation_x_RHS, equation_z_LHS, equation_z_RHS = self(*x_dxs_batch_tuple)

                        # LOSS
                        epoch_val_reconstruction_loss += self.reconstruction_loss_fn(x_dxs_batch_tuple[0], x_hat) / D
                        epoch_val_equation_x_loss += self.equation_x_loss_fn(x_dxs_batch_tuple[-1], equation_x_RHS) / D
                        epoch_val_equation_z_loss += self.equation_z_loss_fn(equation_z_LHS, equation_z_RHS) / self.dim

                    self.loss_history['val_reconstruction'].append(epoch_val_reconstruction_loss.detach().cpu().numpy() / N_val)
                    self.loss_history['val_equation_x'].append(epoch_val_equation_x_loss.detach().cpu().numpy() / N_val)
                    self.loss_history['val_equation_z'].append(epoch_val_equation_z_loss.detach().cpu().numpy() / N_val)


            postfix_dict = {}
            for k, v in self.loss_history.items():
                digits = '{:.1e}'.format(v[-1])
                postfix_dict[k] = digits + trend_verbose(v, lengthscale=10, tolerance=2)

            n_epochs_tqdm.set_postfix(postfix_dict)


class TimeDerivativeAutoencoder(TrajectoryDiscovery):
    def __init__(self, autoencoder:Autoencoder, loss_weights:dict, diff_order:int):
        super(TimeDerivativeAutoencoder, self).__init__()
        self.autoencoder = autoencoder
        
        self.dim = self.autoencoder.latent_dim
        self.diff_order = diff_order
        
        self.reconstruction_loss_fn = MSELoss(reduction='sum')

        self.reconstruction_loss_weight = loss_weights['reconstruction_loss_weight']
        self.reconstruction_dx_loss_weight = loss_weights['reconstruction_dx_loss_weight']
        if self.diff_order == 2:
            self.reconstruction_ddx_loss_weight = loss_weights['reconstruction_ddx_loss_weight']

        self.optimizer = None
        self.loss_history = None

    def forward(self, x:Tensor, dx:Tensor, ddx:Tensor=None):
        # Encode
        z, x_hat = self.autoencoder(x)

        # Encode time derivative
        if self.diff_order == 1:
            dz = self.transform_time_derivative_1st_order(self.autoencoder.encoder, x, dx)
            dx_hat = self.transform_time_derivative_1st_order(self.autoencoder.decoder, z, dz)
            return x_hat, dx_hat, None

        elif self.diff_order == 2:
            dz, ddz = self.transform_time_derivative_2nd_order(self.autoencoder.encoder, x, dx, ddx)
            dx_hat, ddx_hat = self.transform_time_derivative_2nd_order(self.autoencoder.decoder, z, dz, ddz)
            return x_hat, dx_hat, ddx_hat
    

    def fit(self, x, dx, ddx=None, validation_data=None, lr=5e-5, n_epochs=1, batch_size=32):
        if self.optimizer is None: self.optimizer = Adam(params=self.parameters(), lr=lr)

        if self.loss_history is None:
            self.loss_history = {'reconstruction': [], 'reconstruction_dx': []}
            if self.diff_order == 2:
                self.loss_history = {**self.loss_history, **{ 'reconstruction_ddx': []}}

            if validation_data is not None:
                self.loss_history = {**self.loss_history, **{'val_' + k: [] for k in self.loss_history.keys()}}

        n_epochs_tqdm = colortqdm(range(n_epochs), desc='Epoch')

        for epoch in n_epochs_tqdm:
            self.train()

            epoch_reconstruction_loss = 0
            epoch_reconstruction_dx_loss = 0
            epoch_reconstruction_ddx_loss = 0

            N_train = 0
            for i in range(x.size(0) // batch_size):
                if self.diff_order == 1:
                    x_dxs_batch_tuple = (x[i*batch_size:(i+1)*batch_size], dx[i*batch_size:(i+1)*batch_size])
                if self.diff_order == 2:
                    x_dxs_batch_tuple = (x[i*batch_size:(i+1)*batch_size], dx[i*batch_size:(i+1)*batch_size], ddx[i*batch_size:(i+1)*batch_size])

                N_batch, D = x_dxs_batch_tuple[0].size()
                N_train += N_batch

                self.optimizer.zero_grad()
                
                # FORWARD
                x_hat, dx_hat, ddx_hat = self(*x_dxs_batch_tuple)

                # LOSS
                reconstruction_loss = self.reconstruction_loss_fn(x_dxs_batch_tuple[0], x_hat) / D
                reconstruction_dx_loss = self.reconstruction_loss_fn(x_dxs_batch_tuple[1], dx_hat) / D
                if self.diff_order == 2:
                    reconstruction_ddx_loss = self.reconstruction_loss_fn(x_dxs_batch_tuple[2], ddx_hat) / D
                else:
                    reconstruction_ddx_loss = 0

                    
                loss = (
                    self.reconstruction_loss_weight * reconstruction_loss / N_batch
                    + self.reconstruction_dx_loss_weight * reconstruction_dx_loss / N_batch
                )

                if self.diff_order == 2:
                    loss += self.reconstruction_ddx_loss_weight * reconstruction_ddx_loss / N_batch

                # BACKWARD
                loss.backward()
                self.optimizer.step()

                # LOG
                epoch_reconstruction_loss += reconstruction_loss.detach()
                epoch_reconstruction_dx_loss += reconstruction_dx_loss.detach()
                if self.diff_order == 2:
                    epoch_reconstruction_ddx_loss += reconstruction_ddx_loss.detach()
                
            self.loss_history['reconstruction'].append(epoch_reconstruction_loss.cpu().numpy() / N_train)
            self.loss_history['reconstruction_dx'].append(epoch_reconstruction_dx_loss.cpu().numpy() / N_train)
            if self.diff_order == 2:
                self.loss_history['reconstruction_ddx'].append(epoch_reconstruction_ddx_loss.cpu().numpy() / N_train)


            if validation_data is not None:
                with torch.no_grad():
                    self.eval()
                    epoch_val_reconstruction_loss = 0
                    epoch_val_reconstruction_dx_loss = 0
                    epoch_val_reconstruction_ddx_loss = 0
                    N_val = 0
                    for i in range(validation_data[0].size(0) // batch_size):
                        if self.diff_order == 1:
                            x_dxs_batch_tuple = (
                                validation_data[0][i*batch_size:(i+1)*batch_size],
                                validation_data[1][i*batch_size:(i+1)*batch_size]
                            )
                        if self.diff_order == 2:
                            x_dxs_batch_tuple = (
                                validation_data[0][i*batch_size:(i+1)*batch_size],
                                validation_data[1][i*batch_size:(i+1)*batch_size],
                                validation_data[2][i*batch_size:(i+1)*batch_size]
                            )
                        N_batch, D = x_dxs_batch_tuple[0].size()
                        N_val += N_batch
                        # FORWARD
                        x_hat, dx_hat, ddx_hat = self(*x_dxs_batch_tuple)

                        # LOSS
                        epoch_val_reconstruction_loss += self.reconstruction_loss_fn(x_dxs_batch_tuple[0], x_hat) / D
                        epoch_val_reconstruction_dx_loss += self.reconstruction_loss_fn(x_dxs_batch_tuple[1], dx_hat) / D
                        if self.diff_order == 2:
                            epoch_val_reconstruction_ddx_loss += self.reconstruction_loss_fn(x_dxs_batch_tuple[2], ddx_hat) / D

                    self.loss_history['val_reconstruction'].append(epoch_val_reconstruction_loss.detach().cpu().numpy() / N_val)
                    self.loss_history['val_reconstruction_dx'].append(epoch_val_reconstruction_dx_loss.detach().cpu().numpy() / N_val)
                    if self.diff_order == 2:
                        self.loss_history['val_reconstruction_ddx'].append(epoch_val_reconstruction_ddx_loss.detach().cpu().numpy() / N_val)

            postfix_dict = {}
            for k, v in self.loss_history.items():
                digits = '{:.1e}'.format(v[-1])
                postfix_dict[k] = digits + trend_verbose(v, lengthscale=10, tolerance=2)

            n_epochs_tqdm.set_postfix(postfix_dict)