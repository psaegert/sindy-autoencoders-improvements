import numpy as np
import torch
from torch import Tensor


class ActivityClassifier:
    def __init__(self):
        raise NotImplementedError

    def classify_converged(self, history:np.ndarray):
        raise NotImplementedError


class SequentialThresholder(ActivityClassifier):
    def __init__(self, threshold=0.1, interval=500):
        self.threshold = threshold
        self.interval = interval

    def classify_converged(self, history:list):
        history = np.stack(history)
        assert history.shape[0] > 0
        if history.shape[0] % self.interval == 0:
            return (Tensor(np.abs(history[-1]) < self.threshold),) * 2
        # active terms, Terms that would be active in an immediate thresholding event

        return Tensor(np.abs(history[(history.shape[0] // self.interval) * self.interval]) < self.threshold), Tensor(np.abs(history[-1]) < self.threshold)


class PatientTrendAwareThresholder(ActivityClassifier):
    def __init__(self, threshold=0.1, trend_threshold=0.002, patience=1000, delay=0):
        assert patience > 0
        self.threshold = threshold
        self.trend_threshold = trend_threshold
        self.patience = patience
        self.delay = delay

        self.last_overshoot_epoch = None
        self.last_trend_overshoot_epoch = None

    def classify_converged(self, history:list):
        assert len(history) > 0
        if len(history) < max(2, self.delay): return Tensor(np.zeros_like(history[0])), None
        current_values = history[-1]
        current_trend_values = current_values - history[-2]

        if self.last_overshoot_epoch is None:
            self.last_overshoot_epoch = np.full_like(current_values, max(self.delay, 0))
        if self.last_trend_overshoot_epoch is None:
            self.last_trend_overshoot_epoch = np.full_like(current_values, max(self.delay, 0))

        self.last_overshoot_epoch[np.abs(current_values) > self.threshold] = len(history)
        self.last_trend_overshoot_epoch[np.abs(current_trend_values) > self.trend_threshold] = len(history)

        return Tensor(np.logical_and(
            self.last_overshoot_epoch < len(history) - self.patience,
            self.last_trend_overshoot_epoch < len(history) - self.patience)
        ), None


class PatientMomentarilyTrendAwareThresholder(ActivityClassifier):
    def __init__(self, threshold=0.1, trend_threshold=0.002, patience=1000, delay=0):
        assert patience > 0
        self.threshold = threshold
        self.trend_threshold = trend_threshold
        self.patience = patience
        self.delay = delay

        self.last_overshoot_epoch = None

    def classify_converged(self, history:list):
        assert len(history) > 0
        if len(history) < max(2, self.delay): return Tensor(np.zeros_like(history[0])), None
        current_values = history[-1]
        current_trend_values = current_values - history[-2]

        if self.last_overshoot_epoch is None:
            self.last_overshoot_epoch = np.full_like(current_values, max(self.delay, 0))

        self.last_overshoot_epoch[np.abs(current_values) > self.threshold] = len(history)

        return Tensor(np.logical_and(
            self.last_overshoot_epoch < len(history) - self.patience,
            current_trend_values <= self.trend_threshold)), None


class PatientAdaptiveThresholder(ActivityClassifier):
    def __init__(self, threshold=0.1, trend_significance_threshold=2, lengthscale=5, patience=1000, delay=0):
        assert patience > 0
        self.threshold = threshold
        self.trend_significance_threshold = trend_significance_threshold
        self.patience = patience
        self.delay = delay
        self.lengthscale = lengthscale

        self.last_overshoot_epoch = None


    def trend(self, a):
        mean_diff_last_values = np.mean(a[-2*self.lengthscale:-self.lengthscale], axis=0)
        mean_diff_now_values = np.mean(a[-self.lengthscale:], axis=0)
        var_last_values = np.var(a[-2*self.lengthscale:-self.lengthscale], axis=0)
        var_now_values = np.var(a[-self.lengthscale:], axis=0)

        return mean_diff_now_values - mean_diff_last_values, np.sqrt(var_last_values + var_now_values)


    def classify_converged(self, history:list):
        assert len(history) > 0
        if len(history) < max(self.lengthscale, self.delay): return Tensor(np.zeros_like(history[0])), None

        history = np.stack(history[-self.lengthscale:])
        trend_difference, trend_uncertainty = self.trend(history)

        if self.last_overshoot_epoch is None:
            self.last_overshoot_epoch = np.full_like(history[0], max(self.delay, 0))

        self.last_overshoot_epoch[history[-1] > self.threshold] = len(history)

        return Tensor(np.logical_and(self.last_overshoot_epoch >= self.patience, np.abs(trend_difference) < self.trend_significance_threshold * trend_uncertainty)), None



class CurvatureConvergenceClassifier(ActivityClassifier):
    def __init__(self, kernel, lengthscale=100, smooth_lengthscale=5, d_factor=10, dd_factor=100, threshold=0.05):
        self.kernel = kernel[::-1]
        self.lengthscale = lengthscale
        self.smooth_lengthscale = smooth_lengthscale
        self.d_factor = d_factor
        self.dd_factor = dd_factor
        self.threshold = threshold

    def compute_convergence_value(self, timeseries):
        assert len(timeseries) > 2 + self.smooth_lengthscale
        smooth_timeseries = np.convolve(timeseries, np.ones(self.smooth_lengthscale), mode='valid')
        smooth_timeseries_diff = np.diff(smooth_timeseries)

        return (
            np.sum(self.kernel[-len(smooth_timeseries):] * np.abs(smooth_timeseries[-self.lengthscale:]))
            + self.d_factor * np.sum(self.kernel[-len(smooth_timeseries)+1:]*np.abs(smooth_timeseries_diff[-self.lengthscale:]))
            + self.dd_factor * np.sum(self.kernel[-len(smooth_timeseries)+2:]*np.abs(np.diff(smooth_timeseries_diff)[-self.lengthscale:]))
        ) / (
            self.lengthscale
        )
    
    def classify_converged(self, history:list):
        history = np.array(history)
        classification_result = np.empty(history.shape[1:])
        for term in range(history.shape[1]):
            for dim in range(history.shape[2]):
                classification_result[term][dim] = self.compute_convergence_value(history) < self.threshold
        return Tensor(classification_result), None