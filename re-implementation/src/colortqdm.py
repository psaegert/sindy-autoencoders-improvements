
from tqdm import tqdm
from tqdm.utils import _unicode, _text_width
import sys

class colortqdm(tqdm):
    def status_printer(self, file):
        fp = file
        fp_flush = getattr(fp, 'flush', lambda: None)
        if fp in (sys.stderr, sys.stdout):
            sys.stderr.flush()
            sys.stdout.flush()

        def fp_write(s):
            fp.write(_unicode(s))
            fp_flush()

        last_len = [0]

        def print_status(s):
            len_s = _text_width(s)
            fp_write('\r' + s + (' ' * max(last_len[0] - len_s, 0)))
            last_len[0] = len_s

        return print_status
