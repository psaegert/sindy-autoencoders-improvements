import torch.nn as nn


class Autoencoder(nn.Module):
    def __init__(self, encoder, decoder, dim=None):        
        super(Autoencoder, self).__init__()

        if len(list(encoder.parameters())) > 0:
            encoder_output_dim = list(encoder.parameters())[-1].size(0)
            decoder_input_dim = list(decoder.parameters())[0].size(1)
            assert encoder_output_dim == decoder_input_dim

            self.latent_dim = encoder_output_dim
            self.device = encoder[0].weight.device
        else:
            if dim is None:
                raise ValueError("Cannot Fetch Dimensionality From Empty Autoencoder")

            self.latent_dim = dim
            self.device = None
        
        self.encoder = encoder
        self.decoder = decoder
        
    def forward(self, x):
        z = self.encoder(x)
        x_hat = self.decoder(z)
        return z, x_hat

    def to(self, device):
        super().to(device)
        self.device = device
        return self