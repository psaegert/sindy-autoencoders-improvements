import numpy as np

RESET = '\x1b[0m'
RED = '\x1b[91m'
GREEN = '\x1b[92m'

def trend(a, lengthscale=10):
    mean_diff_last_values, var_last_values = np.mean(a[-2*lengthscale:-lengthscale]), np.var(a[-2*lengthscale:-lengthscale])
    mean_diff_now_values, var_now_values = np.mean(a[-lengthscale:]), np.var(a[-lengthscale:])
    return mean_diff_now_values - mean_diff_last_values, np.sqrt(var_last_values + var_now_values)

def trend_verbose(a, lengthscale=10, tolerance=2):
    if len(a) > lengthscale+1:
        trend_difference, trend_uncertainty = trend(a, lengthscale)
        try:
            speed = int(np.floor(np.log10(1e-10 + abs(trend_difference/lengthscale))))
        except (OverflowError, ValueError):
            speed = trend_difference = trend_uncertainty = 0
        sign = '+' if speed > 0 else ''
        if trend_difference < - tolerance * trend_uncertainty:
            return f'|{GREEN}↓{sign}{speed}{RESET}'
        elif trend_difference > tolerance * trend_uncertainty:
            return f'|{RED}↑{sign}{speed}{RESET}'
        else:
            return f'|→--'
    else:
        return '|...'
