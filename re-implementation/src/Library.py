import torch
import numpy as np
import sympy
from itertools import combinations_with_replacement

class Library:
    def __init__(self, dim:int, diff_order:int=1, poly_order:int=3, include_sine=False):
        self.dim = dim
        self.diff_order = diff_order
        self.poly_order = poly_order
        self.include_sine = include_sine

        if self.include_sine:
            self.n_sin_terms = self.dim * self.diff_order
        else:
            self.n_sin_terms = 0

        assert 0 < self.diff_order <= 2

        self.n_independent_variables = self.dim * self.diff_order
        self.variable_combination_indices = self.get_combination_indices()

        self.n_terms = 1 + len(self.variable_combination_indices) + int(include_sine) * self.n_sin_terms

        self.term_weights = self.get_term_weights()
        self.term_weights *= self.n_terms / torch.sum(self.term_weights)


    def get_combination_indices(self):
        combination_indices = []
        # do not count LHS term (highest order differential)
        C = [combinations_with_replacement(range(self.n_independent_variables), i+1) for i in range(self.poly_order)]
        for combinations_of_order_i in C:
            for comb in combinations_of_order_i:
                combination_indices.append(comb)
        
        return combination_indices


    def collect_derivatives(self, z, dz, ddz=None):
        N = z.size(0)

        if self.diff_order == 1:
            return torch.cat([z, dz.reshape(N, -1)], dim=1)

        if self.diff_order == 2:
            assert ddz is not None
            return torch.cat([z, dz.reshape(N, -1), ddz.reshape(N, -1)], dim=1)

    
    def build_snapshot(self, library_variables:torch.Tensor):
        N = library_variables.size(0)

        library_terms = torch.empty(N, self.n_terms, device=library_variables.device)

        # constant term
        library_terms[:, 0] = 1

        # combine terms
        for i, index_combination in enumerate(self.variable_combination_indices):
            library_terms[:, i+1] = torch.prod(library_variables[:, index_combination], dim=1)

        if self.include_sine:
            for i in range(self.n_sin_terms):
                library_terms[:, -self.n_sin_terms + i] = torch.sin(library_variables[:, i])

        # shape (N, n_terms)
        return library_terms

    def get_term_weights(self):
        term_weights = torch.ones(self.n_terms, self.dim)
        for i, index_combination in enumerate(self.variable_combination_indices):
            # number of variables in term plus number of derivatives in term
            term_weights[i + 1, :] += len(index_combination)
            term_weights[i + 1, :] += len(np.array(index_combination)[np.array(index_combination) >= self.dim])

        if self.include_sine:
            for i in range(self.n_sin_terms):
                # one term plus sin()-operation
                term_weights[-self.n_sin_terms + i, :] += 2
                if i >= self.dim:
                    term_weights[-self.n_sin_terms + i, :] += 1
        return term_weights



class VerboseLibrary():
    def __init__(self, library:Library):
        self.library = library

    def collect_derivatives_verbose(self, latex=False):
        z = np.array([f'z_{j+1}' for j in range(self.library.dim)], dtype=object)

        if latex:
            dz = np.array([r'\frac{dz_{' + f'{j+1}' + r'}}{dt}' for j in range(self.library.dim)], dtype=object)
        else:
            dz = np.array([f'dz_{j+1}/dt' for j in range(self.library.dim)], dtype=object)

        if self.library.diff_order == 1:
            return np.concatenate([z], axis=0)

        elif self.library.diff_order == 2:
            if latex:
                ddz = np.array([r'\frac{d^{' + f'{2}' + r'}z_{' + f'{j+1}' + r'}}{dt^{' + f'{2}' + r'}}' for j in range(self.library.dim)], dtype=object)
            else:
                ddz = np.array([f'd^{2}z_{j+1}/dt^{2}' for j in range(self.library.dim)], dtype=object)
        return np.concatenate([z, dz], axis=0)

        
    def build_verbose(self, latex=False):
        library_variables = self.collect_derivatives_verbose(latex=latex)
        
        library_terms = ['1']
        for index_combination in self.library.variable_combination_indices:
            library_terms.append(list(library_variables[list(index_combination)]))

        if self.library.include_sine:
            if latex:
                for j in range(self.library.dim):
                    library_terms.append([r'$\sin(z_{' + f'{j+1}' + r'})$'])
                if self.library.diff_order == 2:
                    for j in range(self.library.dim):
                        library_terms.append([r'$\sin(\dot z_{' + f'{j+1}' + r'})$'])
            else:
                for j in range(self.library.dim):
                    library_terms.append([f'sin(z_{j+1})'])
                if self.library.diff_order == 2:
                    for j in range(self.library.dim):
                        library_terms.append([f'sin(dz_{j+1}/dt)'])

        return library_terms
    

    def build_sympy(self, coefficients=None):
        library_terms = self.build_verbose(latex=True)

        if coefficients is None:
            eq = sympy.Symbol('1')
            for term in library_terms[1:]:
                sympy_term = sympy.Symbol(term[0])
                for variable in term[1:]:
                    sympy_term *= sympy.Symbol(variable)
                eq += sympy_term
            return eq.simplify()
        
        else:
            equations = [None for _ in range(self.library.dim)]
            for d in range(self.library.dim):
                eq = sympy.Symbol('1') * coefficients[0, d]
                for term, coef in zip(library_terms[1:], coefficients[1:, d]):
                    sympy_term = sympy.Symbol(term[0])
                    for variable in term[1:]:
                        sympy_term *= sympy.Symbol(variable)
                    eq += sympy_term * coef
                equations[d] = eq.simplify()
            return equations
