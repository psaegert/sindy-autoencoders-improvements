import os
import numpy as np

import pysr

import torch

import sys
sys.path.append('../src')
sys.path.append('../../src')

sys.path.append('../../src/original')
from example_pendulum import get_pendulum_data

device = 'cpu'

model_names = sorted(os.listdir('./models_pAE'))
model_names

for name in model_names:
    train_data = get_pendulum_data(100)

    shuffle = np.random.permutation(len(train_data['x']))
    x = torch.Tensor(train_data['x'][shuffle]).to(device)
    dx = torch.Tensor(train_data['dx'][shuffle]).to(device)
    ddx = torch.Tensor(train_data['ddx'][shuffle]).to(device)

    del train_data

    if os.path.exists(f'./models_pAE-PySR/{name}'): continue

    model = torch.load(os.path.join('.', 'models_pAE', name)).to(device)

    z = model.autoencoder.encoder(x).detach().cpu().numpy()[:1_000]
    dz, ddz = model.transform_time_derivative_2nd_order(model.autoencoder.encoder, x, dx, ddx)
    dz = dz.detach().cpu().numpy()[:1_000]
    ddz = ddz.detach().cpu().numpy()[:1_000]

    del model

    equations = pysr.pysr(np.concatenate([z, dz], axis=1), ddz, 
        unary_operators=["sin"],
        maxsize=20,
        # parsimony=0,
        annealing=True,
        procs=8,
        progress=True,
        npop=1000,
        equation_file=f'./models_pAE-PySR/{name}'
    )