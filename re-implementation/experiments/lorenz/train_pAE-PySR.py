import os
import numpy as np

import pysr

import torch

import sys
sys.path.append('../src')
sys.path.append('../../src')

sys.path.append('../../src/original')
from example_lorenz import get_lorenz_data

device = 'cpu'

model_names = sorted(os.listdir('./models_pAE'))
model_names

noise_strength = 1e-6

for name in model_names:
    if os.path.exists(f'./models_pAE-PySR/{name}.out1'): continue
    train_data = get_lorenz_data(1024, noise_strength=noise_strength)

    shuffle = np.random.permutation(len(train_data['x']))
    x = torch.Tensor(train_data['x'][shuffle]).to(device)
    dx = torch.Tensor(train_data['dx'][shuffle]).to(device)

    del train_data

    model = torch.load(os.path.join('.', 'models_pAE', name)).to(device)

    z = model.autoencoder.encoder(x).detach().cpu().numpy()[:1_000]
    dz = model.transform_time_derivative_1st_order(model.autoencoder.encoder, x, dx).detach().cpu().numpy()[:1_000]

    del model

    equations = pysr.pysr(z, dz, 
        unary_operators=[],
        maxsize=20,
        # parsimony=0,
        annealing=True,
        procs=8,
        progress=True,
        npop=1000,
        equation_file=f'./models_pAE-PySR/{name}'
    )