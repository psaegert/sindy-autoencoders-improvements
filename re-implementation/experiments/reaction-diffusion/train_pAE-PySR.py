import os
import numpy as np

import pysr

import torch

import sys
sys.path.append('../src')
sys.path.append('../../src')

sys.path.append('../../src/original')
from example_reactiondiffusion import get_rd_data

device = 'cpu'

model_names = sorted(os.listdir('./models_pAE'))
model_names

train_data, _, _ = get_rd_data()

shuffle = np.random.permutation(len(train_data['x']))
x = torch.Tensor(train_data['x'][shuffle]).to(device)
dx = torch.Tensor(train_data['dx'][shuffle]).to(device)

del train_data

for name in model_names:
    if os.path.exists(f'./models_pAE-PySR/{name}.out1'): continue

    model = torch.load(os.path.join('.', 'models_pAE', name)).to(device)

    z = model.autoencoder.encoder(x).detach().cpu().numpy()
    dz = model.transform_time_derivative_1st_order(model.autoencoder.encoder, x, dx).detach().cpu().numpy()

    del model

    equations = pysr.pysr(z, dz, 
        # binary_operators=["mult", "plus"],
        unary_operators=["sin"],
        maxsize=20,
        # parsimony=0,
        annealing=True,
        procs=8,
        npop=1000,
        progress=True,
        verbosity=0,
        equation_file=f'./models_pAE-PySR/{name}',
    )